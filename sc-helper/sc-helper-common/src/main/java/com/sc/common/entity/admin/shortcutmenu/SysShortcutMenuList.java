package com.sc.common.entity.admin.shortcutmenu;


/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
public class SysShortcutMenuList extends SysShortcutMenu {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}