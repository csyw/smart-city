package com.sc.ds.config;

import com.sc.ds.jdbc.DBUtil;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import javax.annotation.PostConstruct;

public abstract class DruidDataSourceConfigurationAbstract extends DataSourceConfigurationAbstract {
    @Autowired
    private Environment environment;


    @PostConstruct
    public void createDefaultDB() {
        String url1 = environment.getProperty("spring.datasource.druid.url");
        String username1 = environment.getProperty("spring.datasource.druid.username");
        String password1 = environment.getProperty("spring.datasource.druid.password");
        String driverClassName1 = environment.getProperty("spring.datasource.druid.driver-class-name");
        String dbDefaultName = environment.getProperty("db-default-name");
        String defaultUrl1 = url1.replace(dbDefaultName,"mysql");

        DBUtil.createDB(driverClassName1,defaultUrl1,username1,password1,dbDefaultName);

        String baselineVersion = environment.getProperty("spring.flyway.baseline-version");
        String baselineOnMigrate = environment.getProperty("spring.flyway.baseline-on-migrate");
        Flyway flyway = Flyway.configure().baselineVersion(baselineVersion).baselineOnMigrate(Boolean.valueOf(baselineOnMigrate)).cleanOnValidationError(false).dataSource(url1, username1, password1).load();
        flyway.migrate();
    }
}
