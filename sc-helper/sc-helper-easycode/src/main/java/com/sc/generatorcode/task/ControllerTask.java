package com.sc.generatorcode.task;

import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public class ControllerTask extends AbstractTask {

    public ControllerTask(String className) {
        super(className);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String postfixName =  super.getName()[0];
        String name = super.getName()[1];

        String entityPackageName = ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName()+"." + name;

        Map<String, String> data = new HashMap<>();
        data.put("BasePackageName", ConfigUtil.getConfiguration().getBasePackage().getBase());
        data.put("ControllerPackageName", ConfigUtil.getConfiguration().getBasePackage().getController());
        data.put("ServicePackageName", ConfigUtil.getConfiguration().getBasePackage().getInterf());
        data.put("EntityPackageName", entityPackageName);
        data.put("Author", ConfigUtil.getConfiguration().getAuthor());
        data.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        data.put("ClassName", postfixName);
        data.put("EntityName", className);
        data.put("EntityNameForCamelCase", StringUtil.firstToLowerCase(className));
        data.put("CorePackageName", ConfigUtil.getConfiguration().getBasePackage().getCorePackageName());
        String filePath = FileUtil.getSourcePath() + StringUtil.package2Path(ConfigUtil.getConfiguration().getBasePackage().getController());
        String fileName = postfixName + "Controller.java";
        data.put("FileName", fileName);
        // 生成Controller文件
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_CONTROLLER, data, filePath,fileName);
    }
}
