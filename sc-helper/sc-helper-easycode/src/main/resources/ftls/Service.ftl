package ${InterfacePackageName};

import ${BasePackageName}.common.service.BaseService;
import ${EntityPackageName}.${ClassName};

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
public interface ${ClassName}Service extends BaseService<${ClassName}>{
}
