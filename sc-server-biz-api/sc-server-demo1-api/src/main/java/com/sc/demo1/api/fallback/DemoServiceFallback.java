package com.sc.demo1.api.fallback;

import com.sc.common.dto.WebResponseDto;
import com.sc.demo1.api.DemoService;
import com.sc.demo1.entity.demo.Demo;
import org.springframework.stereotype.Component;

/**
 * 服务降级配置
 */
@Component
public class DemoServiceFallback implements DemoService {
    @Override
    public WebResponseDto select(String ctx, Demo search) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }
}
