package com.sc.order.entity.order;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-07-15 14:14:39
 * @description:
 */
public class OrderSearch extends Order {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}