package com.sc.workorder.entity;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
public class WoWorkOrderUser extends BaseEntity {
    private Long workOrderTypeId;
    private Long userId;
    private String status;
    private String description;
    private Long projectId;
    private Long companyId;

    public void setWorkOrderTypeId(Long workOrderTypeId) {
        this.workOrderTypeId = workOrderTypeId;
    }

    public Long getWorkOrderTypeId() {
        return workOrderTypeId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}