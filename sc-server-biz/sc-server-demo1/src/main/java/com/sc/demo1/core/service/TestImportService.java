package com.sc.demo1.core.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-07-04 13:40:18
 * @description:
 */
public interface TestImportService {
    WebResponseDto importByExcel(JSONObject jsonObject);
}
