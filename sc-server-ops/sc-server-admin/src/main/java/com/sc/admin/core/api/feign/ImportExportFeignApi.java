package com.sc.admin.core.api.feign;

import com.sc.admin.api.ImportExportService;
import com.sc.admin.core.dao.SysImportExportMapper;
import com.sc.common.annotations.FeignApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author ：wust
 * @date ：Created in 2019/9/7 10:24
 * @description：内部系统调用的接口
 * @version:
 */
@FeignApi
@RestController
public class ImportExportFeignApi implements ImportExportService {
    @Autowired
    private SysImportExportMapper sysImportExportMapper;

    @Override
    @RequestMapping(value = INSERT,method= RequestMethod.POST)
    public WebResponseDto insert(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysImportExport entity) {
        WebResponseDto responseDto = new WebResponseDto();
        int result = sysImportExportMapper.insert(entity);
        responseDto.setObj(result);
        return responseDto;
    }


    @Override
    @RequestMapping(value = SELECT_ONE,method= RequestMethod.POST)
    public WebResponseDto selectOne(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysImportExport search) {
        WebResponseDto responseDto = new WebResponseDto();
        SysImportExport sysImportExport = sysImportExportMapper.selectOne(search);
        responseDto.setObj(sysImportExport);
        return responseDto;
    }


    @Override
    @RequestMapping(value = UPDATE_BY_PRIMARY_KEY_SELECTIVE,method= RequestMethod.POST)
    public WebResponseDto updateByPrimaryKeySelective(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysImportExport entity) {
        WebResponseDto responseDto = new WebResponseDto();
        int count = sysImportExportMapper.updateByPrimaryKeySelective(entity);
        responseDto.setObj(count);
        return responseDto;
    }
}
