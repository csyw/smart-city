/**
 * Created by wust on 2019-11-04 17:59:21
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open.user;


import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.CommonUtil;
import com.sc.common.util.cache.SpringRedisTools;

import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2019-11-04 17:59:21
 * @description:
 *
 */
public abstract class AppUserLoginBase {
    protected abstract SpringRedisTools getSpringRedisTools();

    protected String[] createToken(String loginName) {
        return CommonUtil.generateAppToken(loginName);
    }

    /**
     * 将当前用户绑定到当前上下文
     * @param mapValue
     * @param sysUser
     */
    protected void appendUserToUserContext(final Map<String,Object> mapValue,final SysUser sysUser){
        mapValue.put("user",sysUser);
    }

    /**
     * 将当前用户拥有的项目列表绑定到当前上下文
     * @param mapValue
     * @param userId
     */
    protected void appendProjectToUserContext(final Map<String,Object> mapValue,Long userId){
        JSONArray sysProjects = new JSONArray();
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_USER_ID.getStringValue(),userId);
        Map map = getSpringRedisTools().getMap(key);
        if(CollectionUtil.isNotEmpty(map)) {
            Set keys = map.keySet();
            for (Object o : keys) {
                Map projectMap = map.get(o.toString()) == null ? null : (Map)map.get(o.toString());
                sysProjects.add(JSONObject.toJSON(projectMap));
            }
        }
        mapValue.put("myProjectList",sysProjects);
    }
}
