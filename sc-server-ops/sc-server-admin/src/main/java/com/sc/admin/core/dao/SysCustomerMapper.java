package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.customer.SysCustomer;

/**
 * @author: wust
 * @date: 2019-12-27 11:37:51
 * @description:
 */
public interface SysCustomerMapper extends IBaseMapper<SysCustomer>{
}