package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.datasource.SysDataSource;

/**
 * Created by wust on 2019/6/17.
 */
public interface SysDataSourceMapper  extends IBaseMapper<SysDataSource> {

}
