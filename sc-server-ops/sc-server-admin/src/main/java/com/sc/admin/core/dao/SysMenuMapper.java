package com.sc.admin.core.dao;

import com.sc.admin.dto.MenuTreeDto;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.menu.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DataAccessException;
import java.util.List;


public interface SysMenuMapper  extends IBaseMapper<SysMenu> {
	/**
	 * 平台超级管理员：非白名单大于1级的左侧菜单
	 * @param permissionType 系统id
	 * @param lang 语言
	 * @return
	 */
	List<SysMenu> findAsideMenu4superAdmin(@Param("permissionType")String permissionType,@Param("lang")String lang) throws DataAccessException;


	/**
	 * 平台普通管理员：非白名单大于1级的左侧菜单
	 * @param permissionType
	 * @param lang
	 * @param accountId
	 * @return
	 * @throws DataAccessException
	 */
	List<SysMenu> findAsideMenu4admin(@Param("permissionType")String permissionType,@Param("lang")String lang,@Param("accountId") Long accountId) throws DataAccessException;


	/**
	 * 员工：非白名单大于1级的左侧菜单
	 * @param permissionType 系统id
	 * @param userId 登录的用户id
	 * @param lang 语言
	 * @return
	 */
	List<SysMenu> findAsideMenuByUserId(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("userId")Long userId) throws DataAccessException;


	/**
	 * 获取指定账号拥有到非白名单菜单资源
	 * @param permissionType
	 * @param lang
	 * @param accountId
	 * @return
	 */
	List<MenuTreeDto> findByAccountId(@Param("permissionType")String permissionType, @Param("lang")String lang, @Param("accountId")Long accountId);


	/**
	 * 获取组织架构上指定岗位拥有到非白名单菜单资源
	 * @param organizationId
	 * @return
	 */
	List<MenuTreeDto> findByOrganizationId(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("organizationId")Long organizationId);


	/**
	 * 删除所有
	 * @return
	 * @throws DataAccessException
	 */
	int deleteAll() throws DataAccessException;


	/**
	 * 平台超级管理员：非白名单等于1级的左侧菜单
	 * @param permissionType 权限类型
	 * @param lang 语言
	 * @return
	 */
	List<SysMenu> findOneLevelMenus4superAdmin(@Param("permissionType")String permissionType,@Param("lang")String lang) throws DataAccessException;


	/**
	 * 平台普通管理员：非白名单等于1级的左侧菜单
	 * @param permissionType
	 * @param lang
	 * @param accountId
	 * @return
	 * @throws DataAccessException
	 */
	List<SysMenu> findOneLevelMenus4admin(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("accountId")Long accountId) throws DataAccessException;


	/**
	 * 员工：非白名单等于1级的左侧菜单
	 * @param permissionType 权限类型
	 * @param userId 登录的用户id
	 * @param lang 语言
	 * @return
	 */
	List<SysMenu> findOneLevelMenusByUserId(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("userId")Long userId) throws DataAccessException;



	List<SysMenu> findMenuNameByPermissionType(@Param("permissionType")String permissionType,@Param("lang")String lang) throws DataAccessException;
}
