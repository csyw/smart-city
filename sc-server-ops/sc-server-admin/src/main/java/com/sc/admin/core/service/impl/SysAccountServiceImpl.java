package com.sc.admin.core.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysAccountMapper;
import com.sc.admin.core.dao.SysAccountResourceMapper;
import com.sc.admin.core.dao.SysResourceMapper;
import com.sc.admin.core.service.SysAccountService;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.account.resource.SysAccountResource;
import com.sc.common.entity.admin.resource.SysResource;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: wust
 * @date: 2019-12-27 11:55:31
 * @description:
 */
@Service("sysAccountServiceImpl")
public class SysAccountServiceImpl extends BaseServiceImpl<SysAccount> implements SysAccountService{
    @Autowired
    private SysAccountMapper sysAccountMapper;

    @Autowired
    private SysAccountResourceMapper sysAccountResourceMapper;

    @Autowired
    private SysResourceMapper sysResourceMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysAccountMapper;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        SysAccount account = sysAccountMapper.selectByPrimaryKey(obj);
        if(account != null){
            account.setIsDeleted(1);
            account.setModifyId(ctx.getAccountId());
            account.setModifyName(ctx.getAccountName());
            account.setModifyTime(new Date());
            sysAccountMapper.updateByPrimaryKeySelective(account);
        }
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto setFunctionPermissions(JSONObject jsonObject) {
        WebResponseDto mm = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        Long accountId = jsonObject.getLong("accountId");

        Example example = new Example(SysAccountResource.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("accountId",accountId);
        sysAccountResourceMapper.deleteByExample(example);


        JSONArray jsonArray = jsonObject.getJSONArray("accountResources");
        List<SysAccountResource> list = new ArrayList<>();
        for (Object object : jsonArray) {
            JSONObject j = (JSONObject)object;
            SysAccountResource accountResource = new SysAccountResource();
            accountResource.setAccountId(accountId);
            accountResource.setResourceCode(j.getString("resourceCode"));
            accountResource.setType(j.getString("type"));
            accountResource.setCreaterId(ctx.getAccountId());
            accountResource.setCreaterName(ctx.getAccountName());
            accountResource.setCreateTime(new Date());
            list.add(accountResource);

            // 将所勾选的菜单下面的所有私有白名单权限也自动授予角色
            if(ApplicationEnum.MENU_TYPE_M.getStringValue().equalsIgnoreCase(accountResource.getType())){
                List<SysResource> anonList = sysResourceMapper.findAnonResourcesByMenuId(accountResource.getResourceCode());
                if(!CollectionUtils.isEmpty(anonList)){
                    for(SysResource anonR : anonList){
                        SysAccountResource anon = new SysAccountResource();
                        anon.setAccountId(accountId);
                        anon.setResourceCode(anonR.getCode());
                        anon.setType(ApplicationEnum.MENU_TYPE_R.getStringValue());
                        anon.setCreateTime(new Date());
                        list.add(anon);
                    }
                }
            }
        }

        sysAccountResourceMapper.insertList(list);
        return mm;
    }
}
