package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysTenantMapper;
import com.sc.admin.core.service.SysTenantService;
import com.sc.admin.entity.tenant.SysTenant;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
@Service("sysTenantServiceImpl")
public class SysTenantServiceImpl extends BaseServiceImpl<SysTenant> implements SysTenantService{
    @Autowired
    private SysTenantMapper sysTenantMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysTenantMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
